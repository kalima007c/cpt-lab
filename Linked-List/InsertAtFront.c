#include<stdio.h>
#include<stdlib.h>

struct listnode {
	int data;
	struct listnode *next;
};
typedef struct listnode LN;

void insert_at_front(LN **hptr,int d);
void print(LN *hptr);
int sum(LN *hptr);

int main() {
	LN *head = NULL;
	int temp;

 printf("Enter: \n");  
 
do
{
	printf("=> ");
	scanf("%d",&temp);
	if(temp > 0)
		insert_at_front(&head,temp);
}
while(temp > 0);

	printf("ALL IN LIST => ");
	print(head);
 	printf("\nSUM %d",sum(head));
 	
return 0; 

}

void insert_at_front(LN **hptr,int d) {
	LN *add_node;
	add_node = (LN*)malloc(sizeof(LN));
	add_node->data = d;
	add_node->next = *hptr;
	*hptr = add_node;
}

void print(LN *hptr) {
	while(hptr) {
		printf(" %d ",hptr->data);
		hptr = hptr->next;
	}
}
int sum(LN *hptr) {
	int sum = 0 ; 
	while(hptr) {
		sum += hptr->data;
		hptr = hptr->next;
	}
	return sum;
}
