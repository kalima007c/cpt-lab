#include <stdio.h>
#include <stdlib.h>

struct employee {
    char name[128];
    float salary;
};
typedef struct employee Employee;
FILE *fp;


void createEmployee();
int main() {
    fp = fopen("employee.bin","wb");
    createEmployee();
    fclose(fp);
}
void createEmployee(){
    int employeeCount;
    printf("Employee Count : ");
    scanf("%d",&employeeCount);
        Employee Emp[employeeCount];
    int i;
    for(i = 0; i < employeeCount; i++) {
        printf("Name[%d]: ",i+1);
        fflush(stdin);
            scanf(" %s",&Emp[i].name);
        printf("Salary[%d]: ",i+1);
        fflush(stdin);
            scanf(" %f",&Emp[i].salary);
    }
    printf("employeeCount = %d\n",employeeCount);
    fwrite(&employeeCount,sizeof(int),1,fp);
    fwrite(Emp,sizeof(Employee), 1, fp);
}
